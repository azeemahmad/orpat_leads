<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function socialLogin($social)
    {

        return Socialite::driver($social)->redirect();
    }

    public function handleProviderCallback($social)
    {

        $userSocial = Socialite::driver($social)->user();
        if ($userSocial->getEmail() == null || $userSocial->getEmail() == '') {
            return redirect()->back()->with('error_message', 'Enter your Social login Email!');
        }
        $user=User::where('email',$userSocial->getEmail())->first();
        if( $user){
            $this->guard()->login($user);
            return redirect($this->redirectTo);
        }
        else{
            if($social=='facebook'){
                $scocailTypeLogin=1;
            }
            elseif($social=='google'){
                $scocailTypeLogin=2;
            }
            else{
                $scocailTypeLogin=0;
            }
            $user = User::create([
                'name' => $userSocial->getName(),
                'email' => $userSocial->getEmail(),
                'image' =>$userSocial->avatar,
                'password' => bcrypt(12345678),
                'social_type' => 1,
                'social_login_type'=>$scocailTypeLogin,
            ]);
            $this->guard()->login($user);
            return redirect($this->redirectTo);
        }

    }
}
